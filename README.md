[![Discord](https://img.shields.io/discord/1153159196242620507)](https://discord.gg/zYTvkYCzgB)

# Open Film Database

The Open Film Database is a collection of information on still photography film stocks that can be used for personal and commercial purposes.

## Project Goals

There are several film databases available on the internet, include the [Massive Dev Chart](https://www.digitaltruth.com/devchart.php), [Filmstocks.info](https://filmstocks.info/), [analog film.space](analog film.space), [Filmtypes](https://www.filmtypes.com/), [Photographic Film Index](https://www.filmrescue.com/film-database/), etc., etc., etc.

So why do we need a new one?

Each of the aforementioned databases are proprietary, that is, they can only be utilized to the degree that the creators of the databases allow. Most of them are highly incomplete, offering at best only superficial amounts of data on each film stock—sometimes little more than a name.

As the author of [Crown + Flint](https://crown-and-flint.goodman-wilson.com/), a mobile app for analog photographers, I understand the need for technical film data in a machine-readable format. I know there are other apps out there that could benefit from this data as well. There simply is no suitable source of data to be had, short of scouring the internet for datasheets, lore, and broken links.


The Open Film Database aims to change all that. The goal is to provide an open source, comprehensive, and definitive source of all data available for the widest possible range of still photography film stocks, whether in production or out of production. Anyone is welcome to contribute knowledge, or to use the database for essentially any purpose, including commercial purposes. The only catch is that

 a. If you use the database, you must acknowledge that use with a credit
 b. If you expand upon the database, you make that additional data under the exact same terms.

(see LICENSE for details).

## Current State

What's here now is little more than a sketch and a proposal. There are stubs for many kinds of films that I already know about, but often little more than a name or possibly a box speed.

The exception is my favorite film, a well-documented film—Ilford FP4 Plus. The entry for this film represents a proposal for how to represent film data. You maybe have opinions on this proposal—great! Let's start a conversation [on Discord](https://discord.gg/zYTvkYCzgB), or [mailto:don@goodman-wilson.com?subject=Open%20Film%20Database%20inquiry](contact Don Goodman-Wilson via email).

### Proposed data format

```json
{
  "manufacturerDescription": "ILFORD FP4 PLUS is a medium speed, all-purpose black & white film with very fine grain and outstanding sharpness. It has superb exposure latitude above and below its ISO 125, (meaning it can be both push or pull-processed). This makes FP4 PLUS a highly capable film for most photographic subjects, scenarios and lighting conditions. It particularly excels at highly detailed subjects in good indoor and outdoor lighting conditions and is therefore an ideal film for portraits, fashion, street, product photography, landscapes and architecture. FP4 PLUS can be processed in a wide range of different developers using spiral tanks, deep tanks and automatic processors.",
  "boxSpeed": 125,
  "color": "b&w",
  "spectrum": "panchromatic",
  "process": "b&w",
  "datasheets": {
    "en": "https://www.ilfordphoto.com/amfile/file/download/file/1919/product/686/"
  },
  "latitude": {
    "over": 6,
    "under": 2
  },
  "formats": {
    "135-36": {
      "base": "acetate",
      "baseThickness": 0.125
    },
    "135-24": {
      "base": "acetate",
      "baseThickness": 0.125
    },
    "120": {
      "base": "acetate",
      "baseThickness": 0.110
    },
    "sheet": {
      "base": "polyester",
      "baseThickness": 0.180
    }
  },
  "spectralSensitivity": [
    {
      "wavelength":400,
      "sensitivity": 0.5
    },
    {
      "wavelength":450,
      "sensitivity": 0.75
    },
    {
      "wavelength":500,
      "sensitivity": 0.75
    },
    {
      "wavelength":550,
      "sensitivity": 0.8
    },
    {
      "wavelength":600,
      "sensitivity": 1.0
    },
    {
      "wavelength":650,
      "sensitivity": 0.25
    }
  ],
  "reciprocityFailure": [
    {
      "tStart": 1.0,
      "p": 1.26
    }
  ]
}
```
